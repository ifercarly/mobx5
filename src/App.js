import React, { Component } from 'react'
import TodoHeader from './components/TodoHeader'
import TodoMain from './components/TodoMain'
import TodoFooter from './components/TodoFooter'

export default class App extends Component {
  render() {
    return (
      <div className='todoapp'>
        <TodoHeader />
        <TodoMain />
        <TodoFooter />
      </div>
    )
  }
}
