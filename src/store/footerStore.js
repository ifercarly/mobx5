import { action, observable } from 'mobx'

class FooterStore {
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @observable state = {
    tabs: ['All', 'Active', 'Completed'],
    active: 'All',
  }
  @action.bound updateActive(active) {
    this.state.active = active
  }
}
export default FooterStore
