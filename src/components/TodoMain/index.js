import { inject, observer } from 'mobx-react'
import { Component } from 'react'
import TodoItem from '../TodoItem'

@inject('mainStore')
@observer
export default class TodoMain extends Component {
  componentDidMount() {
    this.props.mainStore.getTodos()
  }
  render() {
    const { renderList, mainRadioStatus, upatePerRadioStatus } = this.props.mainStore
    // !1. 双击显示编辑框
    // !2. 双击编辑框聚焦
    // !3. 双击回填内容
    // !4. 敲回车进行修改
    // !5. ESC 退出编辑
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' checked={mainRadioStatus} onChange={() => upatePerRadioStatus(!mainRadioStatus)} />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          {renderList.map((item) => (
            <TodoItem key={item.id} item={item} />
          ))}
        </ul>
      </section>
    )
  }
}
