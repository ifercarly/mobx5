import classNames from 'classnames'
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'

@inject('mainStore', 'footerStore')
@observer
export default class TodoFooter extends Component {
  componentDidMount() {
    const hs = window.location.hash
    // '' 或 #/ => All
    // #/Active => Active
    // #/Completed => Completed
    /* let active = 'All'
    if (hs === '' || hs === '#/') {
    } else if (hs === '#/Active') {
      active = 'Active'
    } else if (hs === '#/Completed') {
      active = 'Completed'
    } */
    const active = hs === '#/Active' || hs === '#/Completed' ? hs.slice(2) : 'All'
    this.props.footerStore.updateActive(active)
  }
  render() {
    const { clearCompleted, completed, unCompleted } = this.props.mainStore
    const {
      state: { tabs, active },
      updateActive,
    } = this.props.footerStore
    return (
      <footer className='footer'>
        <span className='todo-count'>
          <strong>{unCompleted.length}</strong> item left
        </span>
        <ul className='filters'>
          {tabs.map((item) => (
            <li key={item} onClick={() => updateActive(item)}>
              <a
                className={classNames({
                  selected: active === item,
                })}
                href={`#/${item}`}
              >
                {item}
              </a>
            </li>
          ))}
        </ul>
        {completed.length > 0 && (
          <button className='clear-completed' onClick={clearCompleted}>
            Clear completed
          </button>
        )}
      </footer>
    )
  }
}
