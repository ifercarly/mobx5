## 目标

这次：Mobx5 + class 组件。

下次：Mobx6 + 函数 / Hooks 组件。

## 静态结构

[代码](https://gitee.com/ifercarly/redux-study/tree/01_%E9%9D%99%E6%80%81%E7%BB%93%E6%9E%84/)

## 拆分组件

## 支持装饰器

```bash
yarn add -D react-app-rewired customize-cra @babel/plugin-proposal-decorators
```

`config-overrides.js`

```js
const { override, addDecoratorsLegacy } = require('customize-cra')
module.exports = override(addDecoratorsLegacy())
```

`.babelrc`

https://www.npmjs.com/package/babel-plugin-transform-decorators-legacy

```js
{
  "plugins": [
    ["@babel/plugin-proposal-decorators", { "legacy": true }],
  ]
}
```

`package.json`

```json
{
    "scripts": {
        "start": "react-app-rewired start",
        "build": "react-app-rewired build",
        "test": "react-app-rewired test",
        "eject": "react-scripts eject"
    }
}
```

## 跑通 Mobx

```bash
yarn add mobx@5 mobx-react@5
```

`src/store/todo.js`

```js
import { observable } from 'mobx'

class MainStore {
  @observable todos = [
    { id: 1, name: '吃饭', done: false },
    { id: 2, name: '睡觉', done: true },
  ]
  constructor(rootStore) {
    this.rootStore = rootStore
  }
}

export default MainStore
```

`src/store/index.js`

```js
import MainStore from './todo'
class RootStore {
  constructor() {
    this.MainStore = new MainStore(this)
  }
}
export default RootStore
```

`src/index.js`

```js
import React from 'react'
import ReactDOM from 'react-dom'
import './styles/base.css'
import './styles/index.css'
import App from './App'
import { Provider } from 'mobx-react'
import RootStore from './store'

ReactDOM.render(
  <Provider {...new RootStore()}>
    <App />
  </Provider>,
  document.querySelector('#root')
)
```

`src/components/Main/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'

@inject('MainStore')
@observer
export default class TodoMain extends Component {
  render() {
    const { MainStore } = this.props
    console.log(MainStore.todos)
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          <li className='completed'>
            <div className='view'>
              <input className='toggle' type='checkbox' checked onChange={() => {}} />
              <label>Taste JavaScript</label>
              <button className='destroy'></button>
            </div>
            <input className='edit' value='Create a TodoMVC template' onChange={() => {}} />
          </li>
          <li>
            <div className='view'>
              <input className='toggle' type='checkbox' />
              <label>Buy a unicorn</label>
              <button className='destroy'></button>
            </div>
            <input className='edit' value='Rule the web' onChange={() => {}} />
          </li>
        </ul>
      </section>
    )
  }
}
```

## 列表渲染

`data.json`

```json
{
  "todos": [
    { "id": 1, "name": "吃饭", "done": false },
    { "id": 2, "name": "睡觉", "done": true }
  ]
}
```

`src/components/Main/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import classNames from 'classnames'

@inject('MainStore')
@observer
export default class TodoMain extends Component {
  componentDidMount() {
    this.props.MainStore.getTodos()
  }
  render() {
    const {
      MainStore: { todos },
    } = this.props
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          {todos.map((item) => (
            <li
              className={classNames({
                computed: item.done,
              })}
              key={item.id}
            >
              <div className='view'>
                <input className='toggle' type='checkbox' checked={item.done} onChange={() => {}} />
                <label>{item.name}</label>
                <button className='destroy'></button>
              </div>
              <input className='edit' value='Create a TodoMVC template' onChange={() => {}} />
            </li>
          ))}
        </ul>
      </section>
    )
  }
}
```

`src/store/todo.js`

```js
import { observable, action } from 'mobx'
import request from '../utils/request'

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
}

export default MainStore
```

`src/utils/request.js`

```js
import axios from 'axios'
export default axios.create({
  baseURL: 'http://localhost:8888/todos',
  timeout: 5000,
})
```

## 删除数据

`src/components/Main/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import classNames from 'classnames'

@inject('MainStore')
@observer
export default class TodoMain extends Component {
  componentDidMount() {
    this.props.MainStore.getTodos()
  }
  render() {
    const {
      MainStore: { todos, delTodo },
    } = this.props
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          {todos.map((item) => (
            <li
              className={classNames({
                computed: item.done,
              })}
              key={item.id}
            >
              <div className='view'>
                <input className='toggle' type='checkbox' checked={item.done} onChange={() => {}} />
                <label>{item.name}</label>
                <button className='destroy' onClick={() => delTodo(item.id)}></button>
              </div>
              <input className='edit' value='Create a TodoMVC template' onChange={() => {}} />
            </li>
          ))}
        </ul>
      </section>
    )
  }
}
```

`src/store/todo.js`

```js
import { observable, action, configure } from 'mobx'
import request from '../utils/request'

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
}

export default MainStore
```

## 状态切换

`src/store/todo.js`

```js
import { observable, action, configure } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
}

export default MainStore
```

`src/components/Main/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import classNames from 'classnames'

@inject('MainStore')
@observer
export default class TodoMain extends Component {
  componentDidMount() {
    this.props.MainStore.getTodos()
  }
  render() {
    const {
      MainStore: { todos, delTodo, changeTodoStatus },
    } = this.props
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          {todos.map((item) => (
            <li
              className={classNames({
                computed: item.done,
              })}
              key={item.id}
            >
              <div className='view'>
                <input className='toggle' type='checkbox' checked={item.done} onChange={() => changeTodoStatus(item)} />
                <label>{item.name}</label>
                <button className='destroy' onClick={() => delTodo(item.id)}></button>
              </div>
              <input className='edit' value='Create a TodoMVC template' onChange={() => {}} />
            </li>
          ))}
        </ul>
      </section>
    )
  }
}
```

## 添加数据

`src/components/Header/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'

@inject('MainStore')
@observer
export default class TodoHeader extends Component {
  state = { name: '' }
  handleChangeName = (e) => {
    this.setState({ name: e.target.value })
  }
  handleKeyUpName = (e) => {
    if (e.key === 'Enter') {
      if (this.state.name.trim() === '') return alert('内容不能为空')
      this.props.MainStore.addTodo(this.state.name)
      this.setState({ name: '' })
    }
  }
  render() {
    return (
      <header className='header'>
        <h1>todos</h1>
        <input className='new-todo' value={this.state.name} onChange={this.handleChangeName} placeholder='What needs to be done?' autoFocus onKeyUp={this.handleKeyUpName} />
      </header>
    )
  }
}
```

`src/store/todo.js`

```js
import { observable, action, configure } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
  @action.bound async addTodo(name) {
    await request.post('/', { name, done: false })
    await this.getTodos()
  }
}

export default MainStore
```

## 全选功能

`src/store/todo.js`

```js
import { observable, action, configure, computed } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
  @action.bound async addTodo(name) {
    await request.post('/', { name, done: false })
    await this.getTodos()
  }

  @action.bound async updateTodo(done) {
    const arrPromise = this.todos.map((todo) => {
      return request.patch(`/${todo.id}`, { done })
    })
    await Promise.all(arrPromise)
    await this.getTodos()
  }
  @computed get allStatus() {
    return this.todos.every((item) => item.done)
  }
}

export default MainStore
```

`src/components/Main/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import classNames from 'classnames'

@inject('MainStore')
@observer
export default class TodoMain extends Component {
  componentDidMount() {
    this.props.MainStore.getTodos()
  }
  handleChangeAll = (e) => {
    this.props.MainStore.updateTodo(e.target.checked)
  }
  render() {
    const {
      MainStore: { todos, delTodo, changeTodoStatus, allStatus },
    } = this.props
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' checked={allStatus} onChange={this.handleChangeAll} />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          {todos.map((item) => (
            <li
              className={classNames({
                computed: item.done,
              })}
              key={item.id}
            >
              <div className='view'>
                <input className='toggle' type='checkbox' checked={item.done} onChange={() => changeTodoStatus(item)} />
                <label>{item.name}</label>
                <button className='destroy' onClick={() => delTodo(item.id)}></button>
              </div>
              <input className='edit' value='Create a TodoMVC template' onChange={() => {}} />
            </li>
          ))}
        </ul>
      </section>
    )
  }
}
```

## 双击显示编辑框

`src/components/Item/index.js`

```js
import React, { Component } from 'react'
import classNames from 'classnames'
import { inject, observer } from 'mobx-react'

@inject('MainStore')
@observer
export default class TodoItem extends Component {
  state = {
    isEditing: false,
  }
  handleDblClick = () => {
    this.setState({
      isEditing: !this.state.isEditing,
    })
  }
  render() {
    const {
      item,
      MainStore: { delTodo, changeTodoStatus },
    } = this.props
    const { isEditing } = this.state
    return (
      <li
        className={classNames({
          computed: item.done,
          editing: isEditing,
        })}
        key={item.id}
      >
        <div className='view'>
          <input className='toggle' type='checkbox' checked={item.done} onChange={() => changeTodoStatus(item)} />
          <label onDoubleClick={this.handleDblClick}>{item.name}</label>
          <button className='destroy' onClick={() => delTodo(item.id)}></button>
        </div>
        <input className='edit' value='Create a TodoMVC template' onChange={() => {}} />
      </li>
    )
  }
}
```

## 双击聚焦输入框

`src/components/Item/index.js`

```js
import React, { Component, createRef } from 'react'
import classNames from 'classnames'
import { inject, observer } from 'mobx-react'

@inject('MainStore')
@observer
export default class TodoItem extends Component {
  state = {
    isEditing: false,
  }
  inputRef = createRef()
  handleDblClick = () => {
    this.setState(
      {
        isEditing: !this.state.isEditing,
      },
      () => {
        this.inputRef.current.focus()
      }
    )
  }
  handleBlur = () => {
    this.setState({ isEditing: false })
  }
  render() {
    const {
      item,
      MainStore: { delTodo, changeTodoStatus },
    } = this.props
    const { isEditing } = this.state
    return (
      <li
        className={classNames({
          computed: item.done,
          editing: isEditing,
        })}
        key={item.id}
      >
        <div className='view'>
          <input className='toggle' type='checkbox' checked={item.done} onChange={() => changeTodoStatus(item)} />
          <label onDoubleClick={this.handleDblClick}>{item.name}</label>
          <button className='destroy' onClick={() => delTodo(item.id)}></button>
        </div>
        <input className='edit' value='Create a TodoMVC template' onChange={() => {}} ref={this.inputRef} onBlur={this.handleBlur} />
      </li>
    )
  }
}
```

## 双击回显数据

`src/components/Item/index.js`

```js
import React, { Component, createRef } from 'react'
import classNames from 'classnames'
import { inject, observer } from 'mobx-react'

@inject('MainStore')
@observer
export default class TodoItem extends Component {
  state = {
    isEditing: false,
    // #1
    currentName: '',
  }
  inputRef = createRef()
  handleDblClick = () => {
    this.setState(
      {
        isEditing: !this.state.isEditing,
        // #2
        currentName: this.props.item.name,
      },
      () => {
        this.inputRef.current.focus()
      }
    )
  }
  handleBlur = () => {
    this.setState({ isEditing: false })
  }
  render() {
    const {
      item,
      MainStore: { delTodo, changeTodoStatus },
    } = this.props
    const { isEditing } = this.state
    return (
      <li
        className={classNames({
          computed: item.done,
          editing: isEditing,
        })}
        key={item.id}
      >
        <div className='view'>
          <input className='toggle' type='checkbox' checked={item.done} onChange={() => changeTodoStatus(item)} />
          <label onDoubleClick={this.handleDblClick}>{item.name}</label>
          <button className='destroy' onClick={() => delTodo(item.id)}></button>
        </div>
        {/* #3 */}
        <input className='edit' value={this.state.currentName} onChange={(e) => this.setState(e.target.value)} ref={this.inputRef} onBlur={this.handleBlur} />
      </li>
    )
  }
}
```

## 修改数据

`src/store/todo.js`

```js
import { observable, action, configure, computed } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
  @action.bound async addTodo(name) {
    await request.post('/', { name, done: false })
    await this.getTodos()
  }

  // name => upateTodo all, 可以支持所有的
  @action.bound async updateTodo(done) {
    const arrPromise = this.todos.map((todo) => {
      return request.patch(`/${todo.id}`, { done })
    })
    await Promise.all(arrPromise)
    await this.getTodos()
  }
  @computed get allStatus() {
    return this.todos.every((item) => item.done)
  }

  @action.bound async updateT({ id, key, value }) {
    await request.patch(`/${id}`, { [key]: value })
    await this.getTodos()
  }
}

export default MainStore
```

`src/components/Item/index.js`

```js
import React, { Component, createRef } from 'react'
import classNames from 'classnames'
import { inject, observer } from 'mobx-react'

@inject('MainStore')
@observer
export default class TodoItem extends Component {
  state = {
    isEditing: false,
    // #1
    currentName: '',
  }
  inputRef = createRef()
  handleDblClick = () => {
    this.setState(
      {
        isEditing: !this.state.isEditing,
        // #2
        currentName: this.props.item.name,
      },
      () => {
        this.inputRef.current.focus()
      }
    )
  }
  handleBlur = () => {
    this.setState({ isEditing: false, currentName: '' })
  }
  handleKeyUp = (e) => {
    if (e.key === 'Enter') {
      if (this.state.currentName === '') return
      const { id } = this.props.item
      this.props.MainStore.updateT({ id, key: 'name', value: this.state.currentName })
      this.handleBlur()
    }
  }
  render() {
    const {
      item,
      MainStore: { delTodo, changeTodoStatus },
    } = this.props
    const { isEditing } = this.state
    return (
      <li
        className={classNames({
          computed: item.done,
          editing: isEditing,
        })}
        key={item.id}
      >
        <div className='view'>
          <input className='toggle' type='checkbox' checked={item.done} onChange={() => changeTodoStatus(item)} />
          <label onDoubleClick={this.handleDblClick}>{item.name}</label>
          <button className='destroy' onClick={() => delTodo(item.id)}></button>
        </div>
        {/* #3 */}
        <input
          className='edit'
          value={this.state.currentName}
          onChange={(e) => this.setState({ currentName: e.target.value })}
          ref={this.inputRef}
          onBlur={this.handleBlur}
          onKeyUp={this.handleKeyUp}
        />
      </li>
    )
  }
}
```

==TODO:== ESC 清除。

## 清空已完成

`src/store/todo.js`

```js
import { observable, action, configure, computed } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
  @action.bound async addTodo(name) {
    await request.post('/', { name, done: false })
    await this.getTodos()
  }

  // name => upateTodo all, 可以支持所有的
  @action.bound async updateTodo(done) {
    const arrPromise = this.todos.map((todo) => {
      return request.patch(`/${todo.id}`, { done })
    })
    await Promise.all(arrPromise)
    await this.getTodos()
  }
  @computed get allStatus() {
    return this.todos.every((item) => item.done)
  }

  @action.bound async updateT({ id, key, value }) {
    await request.patch(`/${id}`, { [key]: value })
    await this.getTodos()
  }

  @computed get doned() {
    return this.todos.filter((item) => item.done)
  }
  @action.bound async clearCompleted() {
    const arr = this.doned.map((item) => {
      return request.delete(`/${item.id}`)
    })
    await Promise.all(arr)
    await this.getTodos()
  }
}

export default MainStore
```

`src/components/Footer/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'

@inject('MainStore')
@observer
export default class TodoFooter extends Component {
  render() {
    const { MainStore } = this.props
    return (
      <footer className='footer'>
        <span className='todo-count'>
          <strong>0</strong> item left
        </span>
        <ul className='filters'>
          <li>
            <a className='selected' href='#/'>
              All
            </a>
          </li>
          <li>
            <a href='#/active'>Active</a>
          </li>
          <li>
            <a href='#/completed'>Completed</a>
          </li>
        </ul>
        {MainStore.doned.length > 0 && (
          <button className='clear-completed' onClick={MainStore.clearCompleted}>
            Clear completed
          </button>
        )}
      </footer>
    )
  }
}
```

## 剩余数量的统计

`src/store/todo.js`

```js
import { observable, action, configure, computed } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
  @action.bound async addTodo(name) {
    await request.post('/', { name, done: false })
    await this.getTodos()
  }

  // name => upateTodo all, 可以支持所有的
  @action.bound async updateTodo(done) {
    const arrPromise = this.todos.map((todo) => {
      return request.patch(`/${todo.id}`, { done })
    })
    await Promise.all(arrPromise)
    await this.getTodos()
  }
  @computed get allStatus() {
    return this.todos.every((item) => item.done)
  }

  @action.bound async updateT({ id, key, value }) {
    await request.patch(`/${id}`, { [key]: value })
    await this.getTodos()
  }

  @computed get doned() {
    return this.todos.filter((item) => item.done)
  }
  @computed get unDoned() {
    return this.todos.filter((item) => !item.done)
  }
  @action.bound async clearCompleted() {
    const arr = this.doned.map((item) => {
      return request.delete(`/${item.id}`)
    })
    await Promise.all(arr)
    await this.getTodos()
  }
}

export default MainStore
```

`src/components/Footer/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'

@inject('MainStore')
@observer
export default class TodoFooter extends Component {
  render() {
    const { MainStore } = this.props
    return (
      <footer className='footer'>
        <span className='todo-count'>
          <strong>{MainStore.unDoned.length}</strong> item left
        </span>
        <ul className='filters'>
          <li>
            <a className='selected' href='#/'>
              All
            </a>
          </li>
          <li>
            <a href='#/active'>Active</a>
          </li>
          <li>
            <a href='#/completed'>Completed</a>
          </li>
        </ul>
        {MainStore.doned.length > 0 && (
          <button className='clear-completed' onClick={MainStore.clearCompleted}>
            Clear completed
          </button>
        )}
      </footer>
    )
  }
}
```



## 点击底部按钮高亮

`src/components/Footer/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import classNames from 'classnames'

@inject('MainStore', 'footerStore')
@observer
export default class TodoFooter extends Component {
  handleClick = (active) => {
    this.setState({ active })
  }
  componentDidMount() {
    const hs = window.location.hash
    /* let active = 'All'
    if (hs === '' || hs === '#/') {
    } else if (hs === '#/Active') {
      active = 'Active'
    } else if (hs === '#/Completed') {
      active = 'Completed'
    } */
    const active = hs === '#/Active' || hs === '#/Completed' ? hs.slice(2) : 'All'
    this.props.footerStore.updateActive(active)
  }
  render() {
    const {
      MainStore,
      footerStore: {
        footerData: { tabs, active },
        updateActive,
      },
    } = this.props
    return (
      <footer className='footer'>
        <span className='todo-count'>
          <strong>{MainStore.unDoned.length}</strong> item left
        </span>
        <ul className='filters'>
          {tabs.map((item) => (
            <li key={item} onClick={() => updateActive(item)}>
              <a
                className={classNames({
                  selected: active === item,
                })}
                href={`#/${item}`}
              >
                {item.slice(0, 1).toUpperCase() + item.slice(1)}
              </a>
            </li>
          ))}
        </ul>
        {MainStore.doned.length > 0 && (
          <button className='clear-completed' onClick={MainStore.clearCompleted}>
            Clear completed
          </button>
        )}
      </footer>
    )
  }
}
```

`src/store/filter.js`

```js
import { observable, configure, action } from 'mobx'

configure({ enforceActions: 'observed' })

class FooterStore {
  @observable footerData = {
    tabs: ['All', 'Active', 'Completed'],
    active: 'All',
  }
  constructor(rootStore) {
    this.rootStore = rootStore
  }

  @action.bound updateActive(active) {
    this.footerData.active = active
  }
}

export default FooterStore
```

## 切换功能完成

`src/store/todo.js`

```js
import { observable, action, configure, computed } from 'mobx'
import request from '../utils/request'

configure({ enforceActions: 'observed' })

class MainStore {
  @observable todos = []
  constructor(rootStore) {
    this.rootStore = rootStore
  }
  @action.bound async getTodos() {
    const { data: todos } = await request.get('/')
    this.setTodos(todos)
  }
  @action.bound setTodos(todos) {
    this.todos = todos
  }
  @action.bound async delTodo(id) {
    await request.delete(`/${id}`)
    await this.getTodos()
  }
  @action.bound async changeTodoStatus({ id, done }) {
    await request.patch(`${id}`, { done: !done })
    await this.getTodos()
  }
  @action.bound async addTodo(name) {
    await request.post('/', { name, done: false })
    await this.getTodos()
  }

  // name => upateTodo all, 可以支持所有的
  @action.bound async updateTodo(done) {
    const arrPromise = this.todos.map((todo) => {
      return request.patch(`/${todo.id}`, { done })
    })
    await Promise.all(arrPromise)
    await this.getTodos()
  }
  @computed get allStatus() {
    return this.todos.every((item) => item.done)
  }

  @action.bound async updateT({ id, key, value }) {
    await request.patch(`/${id}`, { [key]: value })
    await this.getTodos()
  }

  @computed get doned() {
    return this.todos.filter((item) => item.done)
  }
  @computed get unDoned() {
    return this.todos.filter((item) => !item.done)
  }
  @action.bound async clearCompleted() {
    const arr = this.doned.map((item) => {
      return request.delete(`/${item.id}`)
    })
    await Promise.all(arr)
    await this.getTodos()
  }
  @computed get renderList() {
    const active = this.rootStore.footerStore.footerData.active
    /* if (active === 'Active') {
      return this.unDoned
    } else if (active === 'Completed') {
      return this.doned
    } else {
      return this.todos
    } */
    return active === 'Active' ? this.unDoned : active === 'Completed' ? this.doned : this.todos
  }
}

export default MainStore
```

`src/components/Main/index.js`

```js
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import TodoItem from '../Item'

@inject('MainStore')
@observer
export default class TodoMain extends Component {
  componentDidMount() {
    this.props.MainStore.getTodos()
  }
  handleChangeAll = (e) => {
    this.props.MainStore.updateTodo(e.target.checked)
  }
  render() {
    const {
      MainStore: { allStatus, renderList },
    } = this.props
    return (
      <section className='main'>
        <input id='toggle-all' className='toggle-all' type='checkbox' checked={allStatus} onChange={this.handleChangeAll} />
        <label htmlFor='toggle-all'>Mark all as complete</label>
        <ul className='todo-list'>
          {renderList.map((item) => (
            <TodoItem key={item.id} item={item} />
          ))}
        </ul>
      </section>
    )
  }
}
```

## 改造 MobX 配置

```bash
yarn add mobx mobx-react-lite
```

`store/index.js`

```js
import { createContext, useContext } from 'react'
import FooterStore from './footerStore'
import MainStore from './mainStore'
class RootStore {
  constructor() {
    this.footerStore = new FooterStore(this)
    this.mainStore = new MainStore(this)
  }
}

const Context = createContext(new RootStore())

export default function useStore() {
  return useContext(Context)
}
```

`store/mainStore.js`、`store/footerStore.js`

`src/index.js`

## 改造类组件为函数组件

`src/App.js`

`src/components/TodoFooter`

`src/components/TodoMain`

`src/components/TodoItem`

`src/components/TodoHeader`